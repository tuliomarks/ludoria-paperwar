import { Injectable } from '@angular/core';
import { ScrollingCamera } from '../components/ScrollingCamera';

//https://phaser.io/examples/v3/view/game-objects/render-texture/dynamic-tilemap-to-render-texture
//https://phaser.io/examples/v3/view/game-objects/tilemap/dynamic/endless-map
//https://phaser.io/examples/v3/view/game-objects/tilemap/collision/csv-map-arcade-physics
//https://medium.com/@michaelwesthadley/modular-game-worlds-in-phaser-3-tilemaps-1-958fc7e6bbd6

@Injectable({
  providedIn: 'root'
})
export class GameplaySceneService extends Phaser.Scene {

  sceneWidthHalf: number;
  sceneHeightHalf: number;

  _controls: any;
  _camera: ScrollingCamera;
  _cursors: Phaser.Types.Input.Keyboard.CursorKeys;
  _keys: Phaser.Types.Input.Keyboard.CursorKeys;
  _debug: any;1

  public constructor() {
    super({ key: 'Scene' });
  }

  public preload() {
    this.load.image('tiles', 'assets/tilesheet/scifi_tilesheet.png');
    this.load.tilemapTiledJSON('map', 'assets/map1.json');

    this.sceneWidthHalf = this.cameras.main.width / 2;
    this.sceneHeightHalf = this.cameras.main.height / 2;
  }

  public create(): void {

    const map = this.add.tilemap('map');
    const tileset = map.addTilesetImage('scifi_tilesheet', 'tiles');

    const skyLayer = map.createStaticLayer('Sky', tileset, 0, 0);
    const objectsLayer = map.createStaticLayer('Objects', tileset, 0, 0);
    const terrainLayer = map.createStaticLayer('Terrain', tileset, 0, 0);

    this._keys = this.input.keyboard.createCursorKeys();

    this._cursors = this.input.keyboard.createCursorKeys();

    let cameraOptions = {
      x: 0,             // x position of the camera (default = 0)
      y: 0,             // y position of the camera (default = 0)
      width: this.game.config.width,        // width of the camera (default = game.config.width)
      height: this.game.config.height,       // height of the camera (default = game.config.height)
      drag: 0.95,        // Reduces the scroll speed per game step in 10%. (default = 0.95)
      minSpeed: 4,       // Bellow this speed value (pixels/second), the scroll is stopped. (default = 4)
    };

    this.cameras.remove(this.cameras.main);
    this._camera = new ScrollingCamera(this, cameraOptions);
    this.cameras.main = this._camera;

    this._controls = new Phaser.Cameras.Controls.FixedKeyControl({
      camera: this._camera,
      left: this._cursors.left,
      right: this._cursors.right,
      up: this._cursors.up,
      down: this._cursors.down,
      speed: 0.5
    });

    // Constrain the camera so that it isn't allowed to move outside the width/height of tilemap
    //this._camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this._debug = this.add.text(10, 30, '', { font: '16px Courier', fill: '#ffffff' });
  }

  public update(time, delta) {

    this._controls.update(delta);
    this._camera.update(time, delta);

    this._debug.setText(this.sceneWidthHalf);
    /*if (this.isLeftTouch()) {
      this._camera.scrollX -= 10 * (delta / 1000)
    } else if (this.isRightTouch()) {
      this._camera.scrollX += 10 * (delta / 1000);
    }

    if (this.isUpTouch()) {
      this._camera.scrollY -= 10 * (delta / 1000);
    } else if (this.isDownTouch()) {
      this._camera.scrollY += 10 * (delta / 1000);
    }*/
  }

  private isUpTouch() {
    return this.input.activePointer.isDown && this.input.activePointer.downY < this.sceneHeightHalf - (this.sceneHeightHalf * 0.5);
  }

  private isDownTouch() {
    return this.input.activePointer.isDown && this.input.activePointer.downY >= this.sceneHeightHalf + (this.sceneHeightHalf * 0.5);
  }

  private isLeftTouch() {
    return this.input.activePointer.isDown && this.input.activePointer.downX < this.sceneWidthHalf - (this.sceneWidthHalf * 0.5);
  }

  private isRightTouch() {
    return this.input.activePointer.isDown && this.input.activePointer.downX >= this.sceneWidthHalf + (this.sceneWidthHalf * 0.5);
  }

}
