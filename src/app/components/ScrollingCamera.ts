/**
 * @fileoverview ScrollingCamera extends the class Phaser.Cameras.Scene2D.Camera of Phaser 3 framework,
 * adding the capacity of vertical scrolling by dragging or using the mouse wheel.
 * @author       Juan Jose Capellan <soycape@hotmail.com>
 * @copyright    2019 Juan Jose Capellan
 * @license      {@link https://github.com/jjcapellan/Phaser3-ScrollingCamera/blob/master/LICENSE | MIT license}
 * @version      1.0.0
 */

/**
 * This type of Phaser camera can be useful to build user interfaces that require scrolling,
 * but without needing scroll bars.
 * The scroll is done by dragging the pointer or using the mouse wheel.
 * @class
 * @extends Phaser.Cameras.Scene2D.Camera
 */
export class ScrollingCamera extends Phaser.Cameras.Scene2D.Camera {

    top: number;
    bottom: number;
    left: number;
    right: number;

    drag: number;
    minSpeed: number;
    _rectangle: Phaser.Geom.Rectangle;

    _startTime: number;
    _endTime: number;
    matrix: any;
    culledObjects: any[];
    _customViewport: any;
    _bounds: any;

    _startY: number;
    _startX: number;
    _endX: number;
    _endY: number;

    _speedX: number;
    _speedY: number;
    /**
     * Creates an instance of ScrollingCamera.
     * @param  {Phaser.Scene} scene 
     * @param  {scrollConfig} scrollConfig Contains scroll parameters 
     * @memberof ScrollingCamera
     */
    constructor(
        scene,
        {
            x = 0,
            y = 0,
            width,
            height,
            drag = 0.95,
            minSpeed = 4,
        }
    ) {
        super(x, y, width, height);
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.width = width || this.scene.game.config.width;
        this.height = height || this.scene.game.config.height;

        this.top = 0;
        this.bottom = this.height;
        this.left = 0;
        this.right = this.width;

        /**
         * Number between 0 and 1.\n
         * Reduces the scroll speed per game step.\n
         * Example: 0.5 reduces 50% scroll speed per game step.
         * @type  {number}
         * @public
         */

        this.drag = drag;
        /**
         * Bellow this speed value (pixels/second), the scroll is stopped.
         * @type  {number}
         * @public
         */
        this.minSpeed = minSpeed;

        this.init();
    }

    init() {
        this.scrollY = this.top || this.y;
        this.scrollX = this.left || this.x;
        this._rectangle = new Phaser.Geom.Rectangle(this.x, this.y, this.width, this.height);
        // Vertical speed in pixels per second
        this._speedX = 0;
        this._speedY = 0;
        // scrollY value when drag action begins
        this._startY = this.scrollY;
        this._startX = this.scrollX;
        // scrollY value when drag action ends
        this._endY = this.scrollY;
        this._endX = this.scrollX;
        // timeStamp when drag action begins
        this._startTime = 0;
        // timeStamp when drag action ends
        this._endTime = 0;
        //// Sets events
        this.setDragEvent();

        this.scene.cameras.addExisting(this);
    }

    setSpeed() {
        let t = this;
        let distanceY = t._endY - t._startY; // pixels
        let distanceX = t._endX - t._startX; // pixels
        let duration = (t._endTime - t._startTime) / 1000; //seconds
        this._speedX = distanceX / duration; // pixels/second
        this._speedY = distanceY / duration; // pixels/second
    }

    setDragEvent() {
        this.scene.input.on('pointermove', this.dragHandler, this);
        this.scene.input.on('pointerup', this.upHandler, this);
        this.scene.input.on('pointerdown', this.downHandler, this);
    }


    downHandler() {
        this._startY = this.scrollY;
        this._startX = this.scrollX;
        this._startTime = performance.now();
    }

    dragHandler(pointer) {
        if (pointer.isDown && this.isOver(pointer)) {
            this._startY = this.scrollY;
            this._startX = this.scrollX;
            this.scrollY -= (pointer.position.y - pointer.prevPosition.y);
            this.scrollX -= (pointer.position.x - pointer.prevPosition.x);
        }
    }

    upHandler() {
        this._endY = this.scrollY;
        this._endX = this.scrollX;
        this._endTime = performance.now();
        this.setSpeed();
    }

    isOver(pointer) {
        return this._rectangle.contains(pointer.x, pointer.y);
    }

    clampScroll() {
        this.scrollY = Phaser.Math.Clamp(this.scrollY, this.top, this.bottom);
        this.scrollX = Phaser.Math.Clamp(this.scrollX, this.left, this.right);
        this._endY = this.scrollY;
        this._endX = this.scrollX;
    }

    public update(time, delta) {
        this.scrollY += Phaser.Math.FloorTo(this._speedY) * (delta / 1000);
        this.scrollX += Phaser.Math.FloorTo(this._speedX) * (delta / 1000);
        this.scrollY = Phaser.Math.FloorTo(this.scrollY);
        this.scrollX = Phaser.Math.FloorTo(this.scrollX);

        this._speedY *= this.drag;
        this._speedX *= this.drag;
        this.clampScroll();

    }

    public destroy() {
        //this.emit(Events.DESTROY, this);
        this.removeAllListeners();
        this.matrix.destroy();
        this.culledObjects = [];
        if (this._customViewport) {
            //  We're turning off a custom viewport for this Camera
            this.sceneManager.customViewports--;
        }
        this._bounds = null;
        this.scene = null;
        this.scaleManager = null;
        this.sceneManager = null;

    }
}

// ************************ TYPE DEFINITIONS *************************************
// *******************************************************************************

/**
 * Contains snap effect parameters
 * @typedef  {object} snapConfig
 * @property  {number} [topMargin = 0] Position y of the first snap point from the top.
 * @property  {number} [padding = 20] Vertical distance in pixels between snap points.
 * @property  {number} [deadZone = 0] % of space between two snap points not influenced by snap effect.\n
 * Example: 0.2 means 20% of middle space between two snap points is free of snap effect.
 */

/**
 * Contains all cameraScroll parameters
 * @typedef  {object} scrollConfig
 * @property  {number} [x = 0] The x position of this camera
 * @property  {number} [y = 0] The y position of this camera
 * @property  {number} [width = Phaser.game.config.width] The width of this camera
 * @property  {number} [height = Phaser.game.config.height] The height of this camera
 * @property  {number} [top = 0] Upper bound of the scroll
 * @property  {number} [bottom = 5000] Lower bound of the scroll
 * @property  {bool} [wheel = false] Does this camera use the mouse wheel?
 * @property  {number} [drag = 0.95] Number between 0 and 1.\n
 * Reduces the scroll speed per game step.\n
 * Example: 0.5 reduces 50% scroll speed per game step
 * @property  {number} [minSpeed] Bellow this speed value (pixels/second), the scroll is stopped
 * @property  {bool} [snap = false] Does this camera use snap points?
 * @property  {snapConfig} [snapConfig] Contains snap effect parameters. Only used if snap parameter is true
 */