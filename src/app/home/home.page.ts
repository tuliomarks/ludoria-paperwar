import { Component, ViewChild, AfterContentInit, HostListener } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameplaySceneService } from './../services/gameplay-scene.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [GameplaySceneService]
})
export class HomePage {

  public gameConfig: Phaser.Types.Core.GameConfig = {
    title: environment.title,
    version: environment.version,
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
      default: 'arcade',
      arcade: {
          gravity: { y: 0 },
          debug: true
      }
  },
  };

  constructor(private _scene: GameplaySceneService) {
  }

  public onGameReady(game: Phaser.Game): void {
    game.scene.add('Scene', this._scene, true);
  }

}
